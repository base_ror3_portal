Welcome to Basic Web Portal
===========================

This is a base Application for a Portal Type System

Gems Used
=========

* Devise (1.1.2) for Authentication
* Declarative_Authorization for Authorization


CSS Frameworks
==============

We use the [BluePrint Framework](http://blueprintcss.org) to give a nice layout, but feel free to use your own


Wiki
====

Please feel free to use the [WIKI](http://wiki.github.com/msimkins/Basic_Web_Portal/) for documenting this

Support Issues
==============

Please create a [Support Issue](http://github.com/msimkins/Basic_Web_Portal/issues) if you believe I have made a major error in the code, or would like a feature added

How to contribute
=================

Please ensure that you provide appropriate spec/test coverage and ensure the documentation is up-to-date.

Bonus points if you perform your changes in a clean topic branch rather than master, and if you create an
issue on GH to discuss your changes. __Pull requests tend to get lost.__

Please also keep your commits __atomic__ so that they are more likely to apply cleanly.

That means that each commit should contain the smallest possible logical change.

Don’t commit two features at once, don’t update the gemspec at the same time you add a feature,
don’t fix a whole bunch of whitespace in a file at the same time you change a few lines, etc, etc.

Maintainers & Contributors
==========================

This project is maintained by Mike Simkins, Contributions are welcome, and you will be mentioned here
