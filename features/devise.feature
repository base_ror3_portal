Feature: Test Authentication Functionality
  In Order to see if I can log in to my App, I need to register an Account

	Scenario: Creating a new account
    		Given I am not authenticated
    		When I go to devise_register
    		And I fill in "user_email" with "someone@example.com"
    		And I fill in "user_password" with "Password123"
    		And I fill in "user_password_confirmation" with "Password123"
    		And I press "Sign up"
    		Then I should see "You have signed up successfully"

#Scenario: Willing to edit my account
#    Given I am a new, authenticated user # beyond this step, your work!
#    When I want to edit my account
#    Then I should see the account initialization form
#    And I should see "Your account has not been initialized yet. Do it now!"

